$(function () {
 $('[data-toggle="tooltip"]').tooltip();
 $('[data-toggle="popover"]').popover();
 $('.carousel').carousel({interval: 2000});
 $('#contacto').on('show.bs.modal', function(e){
  console.log('el modal se esta mostrando');
  $('.contactoBtn').removeClass('btn-outline-seccess');
  $('.contactoBtn').addClass('btn-secondary');
  $('.contactoBtn').prop('disabled', true);
 });
 $('#contacto').on('shown.bs.modal', function(e){
  console.log('el modal se mostró');
 });
 $('#contacto').on('hide.bs.modal', function(e){
  console.log('el modal se esta ocultando');
  $('.contactoBtn').removeClass('btn-secondary');
  $('.contactoBtn').addClass('btn-outline-seccess');
  $('.contactoBtn').prop('disabled', false);
 });
 $('#contacto').on('hidden.bs.modal', function(e){
  console.log('el modal se ocultó');
 });
})